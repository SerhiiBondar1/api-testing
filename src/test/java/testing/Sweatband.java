package testing;

import models.Product;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;


public class Sweatband extends TestBase {

    int id = Integer.parseInt(prop.getProperty("sweatband.id"));
    double price = Double.parseDouble(prop.getProperty("sweatband.price"));
    double newPrice = Double.parseDouble(prop.getProperty("sweatband.new.price"));
    int categoryId = Integer.parseInt(prop.getProperty("sweatband.category.id"));


    @Test
    public void createSweatband(){
        //String endpoint = "http://localhost:80/api_testing/product/create.php";
        String endpoint = prop.getProperty("base.url")
                + prop.getProperty("product.service")
                + prop.getProperty("create.endpoint");
        Product product = new Product(
                prop.getProperty("sweatband.name"),
                prop.getProperty("sweatband.description"),
                price,
                categoryId
        );

        var response = given().body(product).when().post(endpoint).then();
        response.log().body();
    }

    @Test
    public void updateSweatband(){
        //String endpoint = "http://localhost:80/api_testing/product/update.php";
        String endpoint = prop.getProperty("base.url")
                + prop.getProperty("product.service")
                + prop.getProperty("update.endpoint");
        Product product = new Product(
                id,
                prop.getProperty("sweatband.name"),
                prop.getProperty("sweatband.description"),
                newPrice,
                categoryId
                );

                var response = given().body(product).when().put(endpoint).then();
        response.log().body();
    }

    @Test
    public void getSweatband(){
        //String endpoint = "http://localhost:80/api_testing/product/read_one.php";
        String endpoint = prop.getProperty("base.url")
                + prop.getProperty("product.service")
                + prop.getProperty("read.one.endpoint");
        var response =
                given().queryParam("sweatband.id", id).
                        when().get(endpoint).
                        then();
        response.log().body();
    }

    @Test
    public void deleteSweatband(){
        //String endpoint = "http://localhost:80/api_testing/product/delete.php";
        String endpoint = prop.getProperty("base.url")
                + prop.getProperty("product.service")
                + prop.getProperty("delete.endpoint");
        Product product = new Product(id);
        var response = given().body(product).when().delete(endpoint).then();
        response.log().body();
    }
}
