package testing;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;

public class Multivitamin extends TestBase {

    int id = Integer.parseInt(prop.getProperty("multivitamin.id"));
    int numberOfStatusCode = Integer.parseInt(prop.getProperty("multivitamin.status.code"));

    //The word "day's" of body's value of description is difficult for reproducing
    @Test
    public void getMultivitamin() {
        String endpoint = prop.getProperty("base.url") + prop.getProperty("product.service")
                + prop.getProperty("read.one.endpoint");

        given().
                queryParam("id", id).
            when().
                get(endpoint).
            then().
                assertThat().
                statusCode(numberOfStatusCode).
                header("Content-Type", equalTo(prop.getProperty("multivitamin.content.type"))).
                body("id", equalTo(prop.getProperty("multivitamin.id"))).
                body("name", equalTo(prop.getProperty("multivitamin.name"))).
                body("description", equalTo("A daily dose of our Multi-Vitamins fulfills a day’s" +
                        " nutritional needs for over 12 vitamins and minerals.")).
                body("price", equalTo(prop.getProperty("multivitamin.price"))).
                body("category_id", equalTo(prop.getProperty("multivitamin.category.id"))).
                body("category_name", equalTo(prop.getProperty("multivitamin.category.name")));
    }
}
