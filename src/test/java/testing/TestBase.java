package testing;

import org.junit.jupiter.api.BeforeAll;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TestBase {

    public static Properties prop;

    @BeforeAll
    public static void setup() throws IOException {
        prop = new Properties();
        FileReader reader = new FileReader("C:\\Users\\sergi\\Documents\\IdeaProjects\\api-testing\\src\\main\\resources\\local.properties");
        prop.load(reader);
    }
}
