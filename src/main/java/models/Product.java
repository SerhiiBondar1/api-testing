package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class Product {

    @NonNull private int id;
    private String name;
    private String description;
    private double price;
    private int category_id;

    //Used for POST requests
    public Product(String name, String description, double price, int category_id){
        setName(name);
        setDescription(description);
        setPrice(price);
        setCategory_id(category_id);
    }
}
